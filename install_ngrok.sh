#!/usr/bin/bash

wget -o ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip

unzip ngrok.zip

mv ngrok ~/.local/bin/

chmod +x ~/.local/bin/ngrok

echo "Ngrok installed."
