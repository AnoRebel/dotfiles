#!/bin/sh
# Date    : 21/07/2017
# Version : v2.0.1
#########               d8888 8888888b.         ###########
#########              d88888 888   Y88b        ###########
#########             d88P888 888    888        ###########
#########            d88P 888 888   d88P        ###########
#########           d88P  888 8888888P"         ###########
#########          d88P   888 888 T88b          ###########
#########         d8888888888 888  T88b         ###########
#########        d88P     888 888   T88b        ###########
###########################################################

case "$1" in
    lock)
        #i3lock-fancy
        betterlockscreen -l dimblur
        ;;
    logout)
        i3-msg exit
        ;;
    suspend)
        systemctl suspend
        ;;
    hibernate)
        systemctl hibernate
        ;;
    reboot)
        systemctl reboot
        ;;
    shutdown)
        systemctl poweroff
        ;;
    *)
        echo "Usage: $0 {lock|logout|suspend|hibernate|reboot|shutdown}"
        exit 2
esac

exit 0
