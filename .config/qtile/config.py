# -*- coding: utf-8 -*-
#  ____ _____ 
# |  _ \_   _|  Derek Taylor (DistroTube)
# | | | || |    http://www.youtube.com/c/DistroTube
# | |_| || |    http://www.gitlab.com/dwt1/
# |____/ |_|
#        
# A customized config.py for Qtile window manager (http://www.qtile.org)     
# Modified by Derek Taylor (http://www.gitlab.com/dwt1/ )
###########################################################
#########               d8888 8888888b.         ###########
#########              d88888 888   Y88b        ###########
#########             d88P888 888    888        ###########
#########            d88P 888 888   d88P        ###########
#########           d88P  888 8888888P"         ###########
#########          d88P   888 888 T88b          ###########
#########         d8888888888 888  T88b         ###########
#########        d88P     888 888   T88b        ###########
###########################################################

# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from libqtile.config import Key, Screen, Group, Match, Drag, Click, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook, extension

from typing import List  # noqa: F401

# Sets mod key to SUPER/WINDOWS
mod = "mod4"
alt = "mod1"
# Terminal
altTerm = "alacritty"
term = "kitty"
browser = "brave-browser"
# The Qtile config file location
QConfig = "/home/ano/.config/qtile/config.py"

keys = [
    Key(["control", alt], "t",
         lazy.spawn(term),
         desc='Launches My Terminal With Zsh Shell'
         ),
    Key([mod], "w",
         lazy.spawn(browser),
         desc='Browser (Brave)'
         ),
    Key([mod], "e",
        lazy.spawn("nemo"),
        desc='Nemo File manager'
        ),
     ### Window controls
     # Key([mod], "i",
     #     lazy.layout.grow(),
     #     lazy.layout.increase_nmaster(),
     #     desc='Expand window (MonadTall), increase number in master pane (Tile)'
     #     ),
     # Key(
     #     [mod], "l",
     #     lazy.layout.shrink(),
     #     lazy.layout.decrease_nmaster(),
     #     desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
     #     ),
     ### Stack controls
     # Key(
     #     [mod, "shift"], "space",
     #     lazy.layout.rotate(),
     #     lazy.layout.flip(),
     #     desc='Switch which side main pane occupies (XmonadTall)'
     #     ),
     # Key(
     #     [mod], "space",
     #     lazy.layout.next(),
     #     desc='Switch window focus to other pane(s) of stack'
     #     ),
    # Personal Settings
     Key(["control", alt], "k",
        #lazy.spawn("i3lock-fancy"),
        lazy.spawn("betterlockscreen -l dimblur"),
        desc='Fancy screen lock'
        ),
     Key([mod], "Return",
        lazy.spawn(altTerm),
        desc='Alternate terminal (Alacritty)'
        ),
     Key(["control", alt], "m",
        lazy.spawn("mate-control-center"),
        desc='Control Center'
        ),
     # Key(
     #    ["control", "mod1"], "m",
     #    lazy.spawn("cinnamon-settings"),
     #    desc='Control Center'
     #    ),
     # Key(
     #    ["control", "mod1"], "m",
     #    lazy.spawn("xcfe4-settings-manager"),
     #    desc='Control Center'
     #    ),
     Key(["control", "shift"], "Return",
        lazy.spawn("mate-power-preferences"),
        desc='Power Manager'
        ),
     Key(["control", alt], "u",
        lazy.spawn("mate-volume-control"),
        desc='Volume Control'
        ),
     # Key(["control", alt], "a",
     #    lazy.spawn("xfce4-appfinder"),
     #    desc='Xfce4 Appfinder'
     #    ),
     Key([mod, "shift"], "n",
        lazy.spawn(term+" -e cmus"),
        desc='Cmus Music Player'
        ),
     Key([mod, "shift"], "o",
        lazy.spawn(term+" -e nvim "+QConfig),
        desc='Edit Qtile Config'
        ),
     # System
     Key([mod, "control"], "x",
        lazy.spawn("xkill"),
        desc='Absolute Kill'
        ),
     Key([mod], "x",
        lazy.run_extension(
            extension.CommandSet(
                fontsize=14,
                dmenu_height=32,  # Only supported by some dmenu forks
                font="DroidSansMono Nerd Font",
                dmenu_bottom=True,
                dmenu_ignorecase=True,
                background="#191919",
                foreground="#fea63c",
                dmenu_prompt=" System: ",
                selected_background="#00695C",
                selected_foreground="#ECECEC",
                #' lock': 'i3lock-fancy',
                commands={
                    ' lock': 'betterlockscreen -l dimblur',
                    ' logout': 'logoutQtile',
                    ' suspend': 'systemctl suspend',
                    ' hibernate': 'systemctl hibernate',
                    ' reboot': 'systemctl reboot',
                    ' shutdown': 'systemctl poweroff',
                },
            ),
        ),
        desc='System menu'
        ),
     Key([mod, "shift"], "r",
        lazy.restart(),
        desc='Restart Qtile'
        ),
     Key([mod, "shift"], "e",
         lazy.shutdown(),
         desc='Shutdown Qtile'
         ),
     Key([mod], "b",
         lazy.spawn("qtile-cmd -o cmd -f hide_show_bar"),
         desc='Toggle bar visibility'),
     #### Menus
     # Key([mod, "shift"], "d",
     #    lazy.spawn("dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#00695C' -sf '#ECECEC' -fn 'Fira Code:bold:pixelsize=12' -p 'Run: '"),
     #    desc='Dmenu'
     #    ),
     #Key([mod, "shift"], "d",
     Key([mod], "d",
        lazy.run_extension(
            extension.DmenuRun(
                dmenu_prompt="Run: ",
                #dmenu_font="FiraCode Nerd Font:bold:pixelsize=12",
                dmenu_ignorecase=True,
                font="DroidSansMono Nerd Font",
                fontsize=12,
                background="#191919",
                foreground="#fea63c",
                selected_background="#00695C",
                selected_foreground="#ECECEC",
                dmenu_height=28,  # Only supported by some dmenu forks
            ),
        ),
        desc='Dmenu'
        ),
     Key([mod], "m",
        lazy.spawn("morc_menu"),
        desc='Morc Menu'
        ),
     Key([alt], "F2",
        lazy.spawn("gmrun"),
        desc='GMrun'
        ),
     Key([mod], "F11",
        lazy.spawn("rofi -show combi -fullscreen -show-icons"),
        desc='Rofi Fullscreen'
        ),
     Key([mod], "r",
        lazy.spawn("rofi -modi run,window,combi,keys -show drun -show-icons"),
        desc='Rofi'
        ),
     # Screenshot
     Key([], "Print",
        lazy.spawn("scrot '%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f$$(xdg-user-dir PICTURES)'"),
        lazy.spawn("paplay /usr/share/sounds/freedesktop/stereo/camera-shutter.oga"),
        lazy.spawn("notify-send -u low -i camera 'Screenshot saved in Pictures folder"),
        desc='Scrot Screenshot'
        ),
     Key(["control"], "Print",
        lazy.spawn("mate-screenshot"),
        lazy.spawn("paplay /usr/share/sounds/freedesktop/stereo/camera-shutter.oga"),
        desc='Mate Screenshot'
        ),
     Key(["shift"], "Print",
        lazy.spawn("mate-screenshot -i"),
        desc='Mate Screenshot interactive'
        ),
     Key(["control", "shift"], "Print",
        lazy.spawn("shutter"),
        desc='Shutter'
        ),
     # Audio Settings
     Key([], "XF86AudioRaiseVolume",
        lazy.spawn("amixer -D pulse sset Master '5%+'"),
        lazy.spawn("notify-send -u low -i audio-volume-medium 'Audio' 'Volume Increased'"),
        desc='Add Volume by 5%'
        ),
     Key([], "XF86AudioLowerVolume",
        lazy.spawn("amixer -D pulse sset Master '5%-'"),
        lazy.spawn("notify-send -u low -i audio-volume-medium 'Audio' 'Volume Decreased'"),
        desc='Lower Volume by 5%'
        ),
     Key([], "XF86AudioMute",
        lazy.spawn("amixer -D pulse set Master toggle"),
        lazy.spawn("notify-send -u low -i audio-volume-medium 'Audio' 'Volume Toggled'"),
        desc='Mute Sound'
        ),
     # Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
     # Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
     # Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
     # Player Controls
     #https://github.com/acrisci/playerctl/
     Key([], "XF86AudioPlay",
        lazy.spawn("playerctl play-pause"),
        lazy.spawn("notify-send -u low -i media-playback-start 'Player' 'Play/Pause Toggled'"),
        desc='Play/Pause'
        ),
     Key([], "XF86AudioNext",
        lazy.spawn("playerctl next"),
        lazy.spawn("notify-send -u low -i media-skip-forward 'Player' 'Next'"),
        desc='Play Next'
        ),
     Key([], "XF86AudioPrev",
        lazy.spawn("playerctl previous"),
        lazy.spawn("notify-send -u low -i media-skip-backward 'Player' 'Previous'"),
        desc='Play Prev'
        ),
     Key([], "XF86AudioStop",
        lazy.spawn("playerctl stop"),
        lazy.spawn("notify-send -u low -i media-playback-stop 'Player' 'Stopped'"),
        desc='Player Stop'
        ),
     # Toggle Touchpad
     Key([], "XF86TouchpadToggle",
        lazy.spawn("toggleTouchpad"),
        lazy.spawn("notify-send -u low -i mouse 'TouchPad' 'TouchPad Toggled'"),
        desc='Toggle Touchpad'
        ),
     # Backlight
     Key([], "XF86MonBrightnessUp",
        lazy.spawn("xbacklight -inc 20"),
        lazy.spawn("notify-send -u low -i display 'Display' 'Brightness Increased'"),
        desc='Increase brightness by 20%'
        ),
     Key([], "XF86MonBrightnessDown",
        lazy.spawn("xbacklight -dec 20"),
        lazy.spawn("notify-send -u low -i display 'Display' 'Brightness Decreased'"),
        desc='Decrease brightness by 20%'
        ),
     # Windows
     Key([mod], "space",
        lazy.window.toggle_floating(),
        desc='Toggle floating window'
        ),
     Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc='Toggle Fullscreen window'
        ),
     Key([mod, "shift"], "q",
        lazy.window.kill(),
        desc='Kill Active Window'
        ),
     # Window Movement
     Key([alt, "shift"], "n",
        lazy.layout.normalize(),
        desc='Restore all windows to their default size ratios'
        ),
     Key([mod], "k",
        lazy.layout.up(),
        desc='Change Focus Up'
        ),
     Key([mod], "j",
        lazy.layout.down(),
        desc='Change Focus Down'
        ),
     Key([mod], "h",
        lazy.layout.left(),
        desc='Change Focus Left'
        ),
     Key([mod], "l",
        lazy.layout.right(),
        desc='Change Focus Right'
        ),
     # Arrow Keys
     Key([mod], "Up",
        lazy.layout.up(),
        desc='Change Focus Up'
        ),
     Key([mod], "Down",
        lazy.layout.down(),
        desc='Change Focus Down'
        ),
     Key([mod], "Left",
        lazy.layout.left(),
        desc='Change Focus Left'
        ),
     Key([mod], "Right",
        lazy.layout.right(),
        desc='Change Focus Right'
        ),
     # Layouts
     Key([mod], "Tab",
        lazy.next_layout(),
        desc='Toggle through layouts'
        ),
     # Treetab Layout
     # Vim Keys Movement
     Key([mod, "control"], "k",
         lazy.layout.section_up(),
         desc='Move up a section in treetab'
         ),
     Key([mod, "control"], "j",
         lazy.layout.section_down(),
         desc='Move down a section in treetab'
         ),
     # Arrow Keys Movement
     Key([mod, "control"], "Up",
         lazy.layout.section_up(),
         desc='Move up a section in treetab'
         ),
     Key([mod, "control"], "Down",
         lazy.layout.section_down(),
         desc='Move down a section in treetab'
         ),
     # Bsp, Columns Layout
     # Vim Keys Movement
     Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc='Move Bsp, Columns type windows Down'
        ),
     Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc='Move Bsp, Columns type windows Up'
        ),
     Key([mod, "shift"], "h",
        lazy.layout.shuffle_left(),
        desc='Move Bsp, Columns type windows Left'
        ),
     Key([mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        desc='Move Bsp, Columns type windows Right'
        ),
     # Bsp type Only Flip
     # Arrow Keys Movement
     Key([mod, "shift"], "Down",
        lazy.layout.shuffle_down(),
        desc='Move Bsp, Columns type windows Down'
        ),
     Key([mod, "shift"], "Up",
        lazy.layout.shuffle_up(),
        desc='Move Bsp, Columns type windows Up'
        ),
     Key([mod, "shift"], "Left",
        lazy.layout.shuffle_left(),
        desc='Move Bsp, Columns type windows Left'
        ),
     Key([mod, "shift"], "Right",
        lazy.layout.shuffle_right(),
        desc='Move Bsp, Columns type windows Right'
        ),
     # Vim Keys Flip
     Key([mod, "control"], "j",
        lazy.layout.flip_down(),
        desc='Flip Bsp type window Down'
        ),
     Key([mod, "control"], "k",
        lazy.layout.flip_up(),
        desc='Flip Bsp type window Up'
        ),
     Key([mod, "control"], "h",
        lazy.layout.flip_left(),
        desc='Flip Bsp type window Left'
        ),
     Key([mod, "control"], "l",
        lazy.layout.flip_right(),
        desc='Flip Bsp type window Right'
        ),
     # Arrow Keys Flip
     Key([mod, "control"], "Down",
        lazy.layout.flip_down(),
        desc='Flip Bsp type window Down'
        ),
     Key([mod, "control"], "Up",
        lazy.layout.flip_up(),
        desc='Flip Bsp type window Up'
        ),
     Key([mod, "control"], "Left",
        lazy.layout.flip_left(),
        desc='Flip Bsp type window Left'
        ),
     Key([mod, "control"], "Right",
        lazy.layout.flip_right(),
        desc='Flip Bsp type window Right'
        ),
     # Bsp, Column Split
     Key([mod], "q",
        lazy.layout.toggle_split(),
        desc='Bsp, Column window type toggle between split and unsplit sides of stack'
        ),
     # Resize
     Key([alt, "shift"], "j",
        lazy.layout.grow_down(),
        desc='Resize Bsp, Columns type window Down'
        ),
     Key([alt, "shift"], "k",
        lazy.layout.grow_up(),
        desc='Resize Bsp, Columns type window Up'
        ),
     Key([alt, "shift"], "h",
        lazy.layout.grow_left(),
        desc='Resize Bsp, Columns type window Left'
        ),
     Key([alt, "shift"], "l",
        lazy.layout.grow_right(),
        desc='Resize Bsp, Columns type window Right'
        ),
     Key([alt, "shift"], "Down",
        lazy.layout.grow_down(),
        desc='Resize Bsp, Columns type window Down'
        ),
     Key([alt, "shift"], "Up",
        lazy.layout.grow_up(),
        desc='Resize Bsp, Columns type window Up'
        ),
     Key([alt, "shift"], "Left",
        lazy.layout.grow_left(),
        desc='Resize Bsp, Columns type window Left'
        ),
     Key([alt, "shift"], "Right",
        lazy.layout.grow_right(),
        desc='Resize Bsp, Columns type window Right'
        ),
     # Monad Layout
     Key(
        [mod, "shift"], "m",
        lazy.layout.maximize(),
        desc='Toggle window between its minimum and maximum sizes'
        ),
     # Vim Keys Movement
     Key([mod, "shift"], "h",
        lazy.layout.swap_left(),
        desc='Move Monad type windows Left'
        ),
     Key([mod, "shift"], "l",
        lazy.layout.swap_right(),
        desc='Move Monad type windows Right'
        ),
     Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc='Move Monad type windows Up'
        ),
     Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc='Move Monad type windows Down'
        ),
     # Arrow Keys Movement
     Key([mod, "shift"], "Up",
        lazy.layout.shuffle_up(),
        desc='Move Monad type windows Up'
        ),
     Key([mod, "shift"], "Left",
        lazy.layout.swap_left(),
        desc='Move Monad type windows Left'
        ),
     Key([mod, "shift"], "Right",
        lazy.layout.swap_right(),
        desc='Move Monad type windows Right'
        ),
     Key([mod, "shift"], "Down",
        lazy.layout.shuffle_down(),
        desc='Move Monad type windows Down'
        ),
     # Resize
     Key([alt, "shift"], "g",
        lazy.layout.grow(),
        desc='Grow Monad type windows'
        ),
     Key([alt, "shift"], "s",
        lazy.layout.shrink(),
        desc='Shrink Monad type windows'
        ),
     # Monad Flip
     Key([mod], "q",
        lazy.layout.flip(),
        desc='Monad window type flip'
        ),
     # Workspace/Groups
     Key(
        [alt], "Tab",
        lazy.screen.toggle_group(),
        desc='Toggle last visited group/workspace'
        ),
     Key(
        [alt, "control"], "Right",
        lazy.screen.next_group(),
        desc='Move to group/workspace on the right'
        ),
     Key(
        [alt, "control"], "Left",
        lazy.screen.prev_group(),
        desc='Move to group/workspace on the left'
        ),
     # Scratchpad
     Key(
        [mod, "shift"], "u",
        lazy.group[""].dropdown_toggle("dropdown"),
        desc='Scratchpad Dropdown terminal'
        ),
]

groups = [
    Group("1: ", matches=[Match(wm_class=["Nemo", "nemo", "Caja", "caja"])],
          layout='tile'),
    Group("2: ", matches=[Match(wm_class=["Xed", "xed", "Gedit", "gedit",
                                             "Sublime", "sublime_text", "Atom",
                                             "atom", "Code", "code"])],
          layout='monadtall'),
    Group("3: 🌎", matches=[Match(wm_class=["Firefox", "firefox",
                                              "Brave-Browser",
                                              "brave-browser"])],
          layout='monadtall'),
    Group("4: ", matches=[Match(wm_class=["Vlc", "vlc", "Celluloid",
                                             "celluloid"])], layout='zoomy'),
    Group("5: ", matches=[Match(wm_class=["feh", "eog", "Eog", "Pinta",
                                             "pinta"])], layout='matrix'),
    Group("6: λ", matches=[Match(wm_class=["Rhythmbox", "rhythmbox"])],
          layout='treetab'),
    Group("7: ", matches=[Match(wm_class=["Thunderbird", "thunderbird"])],
          layout='monadwide'),
    # ScratchPad("", [
    #     # define a drop down terminal.
    #     DropDown("dropdown", term, x=0.3, y=0.2, width=0.7, height=0.5, opacity=0.9) ]
    # ),
]

for i, group in enumerate(groups, 1):
    keys.extend([
        # Switch to another group
        Key([mod], str(i),
            lazy.group[group.name].toscreen(),
            desc='Switch to group'
            ),
        # Send current window to another group
        Key([mod, "control"], str(i),
            lazy.window.togroup(group.name),
            desc='Move window to group'
            ),
        # Send and switch current window to another group
        Key([mod, "shift"], str(i),
            lazy.window.togroup(group.name, switch_group=True),
            desc='Move window and switch to group'
            ),
    ])

##### DEFAULT THEME SETTINGS FOR LAYOUTS #####
layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": "00F2FF",
    "border_normal": "1D2330",
}

##### THE LAYOUTS #####
layouts = [
    # Try more layouts by unleashing below layouts.
    layout.Zoomy(**layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Max(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.MonadTall(change_size=6, **layout_theme),
    layout.MonadWide(change_size=6, **layout_theme),
    layout.Stack(num_stacks=3, **layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.TreeTab(
         font = "Anonymice Nerd Font",
         fontsize = 11,
         fontshadow = "00FF33",
         sections = ["1: ", "2: "],
         section_fontsize = 12,
         bg_color = "202040",
         active_bg = "028C63",
         active_fg = "ECECEC",
         inactive_bg = "222831",
         inactive_fg = "A0A0A0",
         padding_y = 5,
         section_top = 10,
         panel_width = 180
         ),
    layout.Floating(**layout_theme)
]

##### COLORS #####
colors = [["#282a36", "#282a36"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ECECEC", "#ECECEC"], # font color for group names
          ["#028C63", "#028C63"], # border line color for current tab
          ["#8d62a9", "#8D62A9"], # border line color for other tab and odd widgets
          ["#058773", "#058773"], # color for the even widgets
          ["#E1ACFF", "#e1acff"]] # window name

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="DroidSansMono Nerd Font",
    fontsize = 13,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

##### MOUSE CALLBACKS ######
def open_lyrics(qtile):
    qtile.cmd_spawn("kitty -e cmus-lyrics")

widgets_list = [
    widget.Sep(
        linewidth = 0,
        padding = 6,
        foreground = colors[2],
        background = colors[0]
        ),
    widget.GroupBox(font="FiraCode Nerd Font",
        fontsize = 11,
        margin_y = 3,
        margin_x = 0,
        padding_y = 5,
        padding_x = 5,
        borderwidth = 3,
        active = colors[2],
        inactive = colors[2],
        rounded = False,
        highlight_color = colors[1],
        highlight_method = "line",
        this_current_screen_border = colors[3],
        this_screen_border = colors [4],
        other_current_screen_border = colors[0],
        other_screen_border = colors[0],
        foreground = colors[2],
        background = colors[0]
        ),
   widget.Sep(
        linewidth = 0,
        padding = 40,
        foreground = colors[2],
        background = colors[0]
        ),
   widget.WindowName(
        foreground = colors[6],
        background = colors[0],
        fontsize = 12,
        padding = 0
        ),
   widget.TextBox(
        text='',
        background = colors[0],
        foreground = colors[5],
        padding=-8,
        fontsize=46
        ),
   widget.TextBox(
        text="",
        padding = 2,
        foreground=colors[2],
        background=colors[5],
        fontsize=16
    ),
   #  widget.Cmus(
        #  play_color="00FF33",
        #  noplay_color="19F409",
        #  update_interval = 0.5,
        #  foreground = colors[2],
        #  background = colors[5],
        #  fontsize = 12,
        #  mouse_callbacks = {'Button3': lambda qtile: qtile.cmd_spawn(term + ' -e cmus-lyrics')}
        #  ),
   widget.CPU(
       format = '{load_percent}%',
       background = colors[5],
       foreground = colors[2],
       fontsize = 12,
       padding = 5,
       mouse_callbacks = {'Button1': lambda qtile: qtite.cmd_spawn("~/.config/polybar/scripts/notify-cpu")}
   ),
   widget.TextBox(
        text='',
        background = colors[5],
        foreground = colors[4],
        padding=-7,
        fontsize=46
        ),
   widget.TextBox(
        text=" ",
        foreground=colors[2],
        background=colors[4],
        padding = 2,
        fontsize=14
        ),
   widget.Memory(
        format = '{MemUsed}M - {SwapUsed}M',
        foreground = colors[2],
        background = colors[4],
        fontsize = 12,
        padding = 5,
        mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn("~/.config/polybar/scripts/notify-memory")}
        ),
   widget.TextBox(
        text='',
        background = colors[4],
        foreground = colors[5],
        padding=-7,
        fontsize=46
        ),
   widget.TextBox(
        text="  :",
        foreground=colors[2],
        background=colors[5],
        fontsize = 12,
        padding = 0
        ),
   widget.DF(
        # partition='/',
        visible_on_warn=False,
        format='({uf}{m}|{r:.0f}%)',
        foreground = colors[2],
        background = colors[5],
        fontsize = 12,
        padding = 5
        ),
   widget.TextBox(
        text="  :",
        foreground=colors[2],
        background=colors[5],
        fontsize = 12,
        padding = 0
        ),
   widget.DF(
        partition='/home',
        visible_on_warn=False,
        format='({uf}{m}|{r:.0f}%)',
        foreground = colors[2],
        background = colors[5],
        fontsize = 12,
        padding = 5
        ),
   widget.TextBox(
        text='',
        background = colors[5],
        foreground = colors[4],
        padding=-7,
        fontsize=46
        ),
   widget.CurrentLayoutIcon(
        custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
        foreground = colors[0],
        background = colors[4],
        fontsize = 12,
        padding = 0,
        scale=0.7
        ),
   widget.CurrentLayout(
        foreground = colors[2],
        background = colors[4],
        fontsize = 12,
        padding = 5
        ),
   widget.TextBox(
        text='',
        background = colors[4],
        foreground = colors[5],
        padding=-7,
        fontsize=46
        ),
   widget.Clock(
        foreground = colors[2],
        background = colors[5],
        fontsize = 12,
       format="%a, %b %d  [ %H:%M ]"
        ),
   widget.Sep(
        linewidth = 0,
        padding = 10,
        foreground = colors[0],
        background = colors[5]
        ),
   widget.Systray(
        background=colors[0],
        padding = 5
        ),
   widget.QuickExit(
       background = colors[0],
       foreground = "#FF5252",
       default_text = "  ",
       countdown_format = '[{}]'
   ),
]

screens = [
    Screen(
        top=bar.Bar(
            widgets=widgets_list,
            opacity=0.95,
            size=26,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    # My window setup
    {'role': 'setup'},
    {'role': 'pop-up'},
    {'wmclass': 'Wine'},
    {'wname': 'i3_help'},
    {'wname': 'Kernels'},
    {'wname': 'BashTOP'},
    {'wmclass': 'qt5ct'},
    {'wmclass': '^Gpick$'},
    {'wmclass': 'GParted'},
    {'wmclass': 'PulseAudio'},
    {'role': '^Preferences$'},
    {'wmclass': 'Gnome-disks'},
    {'wmclass': 'Simple-scan'},
    {'wmclass': 'Imagewriter'},
    {'wmclass': 'Lxappearance'},
    {'wmclass': 'Font-manager'},
    {'wmclass': 'Mintstick.py'},
    {'wmclass': 'Blueberry.py'},
    {'wmclass': 'gimp'},
    {'wmclass': 'Usb-creator-gtk'},
    {'wmclass': 'Gnome-calculator'},
    {'wmclass': 'Lightdm-settings'},
    {'wmclass': 'Moving'},
    {'wmclass': 'Xfce4-taskmanager'},
    {'wmclass': 'Copying'},
    {'wmclass': 'Deleting'},
    {'wmclass': '^Gnome-font-viewer$'},
    {'wmclass': 'script-fu'},
    {'wmclass': 'mate-control-center'},
    {'wmclass': 'Xfce4-settings-manager'},
    {'role': '^gimp-toolbox-color-dialog$'},
    {'wmclass': 'mate-notification-daemon'},
])

auto_fullscreen = True
focus_on_window_activation = "smart"

### Has error need fix
# @hook.subscribe.screen_change
# def restart_on_randr(qtile, ev):
#     lazy.spawn("notify-send 'Display' 'Screen Configuration changed'")
#     qtile.cmd_restart()
# Another trial
# @hook.subscribe.screen_change
# def set_screens(qtile, event):
#     logger.debug("Handling event: {}".format(event))
#     subprocess.run(["autorandr", "--change"])
#     qtile.restart()

##### STARTUP APPLICATIONS #####
@hook.subscribe.startup_once
def start_once():
    print('Called')
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
