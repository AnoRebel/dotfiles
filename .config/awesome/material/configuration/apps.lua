local filesystem = require('gears.filesystem')

-- Thanks to jo148 on github for making rofi dpi aware!
local with_dpi = require('beautiful').xresources.apply_dpi
local get_dpi = require('beautiful').xresources.get_dpi
local rofi_command = 'env rofi -dpi ' .. get_dpi() .. ' -width ' .. with_dpi(400) .. ' -show drun -theme ' .. filesystem.get_configuration_dir() .. '/configuration/rofi.rasi -run-command "/bin/bash -c -i \'shopt -s expand_aliases; {cmd}\'"'
--local rofi_command = 'env /usr/local/bin/rofi -dpi ' .. get_dpi() .. ' -width ' .. with_dpi(400) .. ' -show drun -theme ' .. filesystem.get_configuration_dir() .. '/configuration/rofi.rasi -run-command "/bin/bash -c -i \'shopt -s expand_aliases; {cmd}\'"'

return {
  -- List of apps to start by default on some actions
  default = {
    terminal = 'env kitty',
    rofi = rofi_command,
    lock = 'i3lock-fancy',
    quake = 'alacritty',
    screenshot = '~/.config/awesome/configuration/utils/screenshot -m',
    region_screenshot = '~/.config/awesome/configuration/utils/screenshot -r',
    delayed_screenshot = 'sleep 10 ; ~/.config/awesome/configuration/utils/screenshot -r',
    
    -- Editing these also edits the default program
    -- associated with each tag/workspace
    browser = 'env brave-browser',
    editor = 'code', -- gui text editor
    -- editor = 'subl', -- gui text editor
    social = 'env geary',
    game = rofi_command,
    files = 'nemo',
    --files = 'caja',
    music = rofi_command
  },
  -- List of apps to start once on start-up
  run_on_start_up = {
    'feh --bg-fill ~/Pictures/Wallpapers/kyojin.png',
    '/usr/bin/compton --config ' .. filesystem.get_configuration_dir() .. '/configuration/compton.conf',
    'numlockx on', -- enable numlock
    'unclutter', -- Mouse cursor cleanup,
    'mate-settings-daemon', -- Settings daemon
    -- Tray Apps
    'nm-applet --indicator', -- wifi
    'mintupdate-launcher', -- Update manager
    'redshift-gtk', -- Color filter,
    --'mate-media', -- Mounted media,
    --'blueberry-tray', -- Blueetooth,
    'mate-power-manager', -- Power manager
    'ibus-daemon --xim --daemonize', -- Ibus daemon for keyboard
    'pasystray', -- Audio
    'clipit', -- Clipboard management
    '/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon --start --components=pkcs11,secrets,ssh,gpg)', -- credential manager
    -- Add applications that need to be killed between reloads
    -- to avoid multipled instances, inside the awspawn script
    '~/.config/awesome/configuration/awspawn' -- Spawn "dirty" apps that can linger between sessions
  }
}
