#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar
# if all your bars have ipc enabled, you can also use
# polybar-msg cmd quit

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

echo "---" | tee -a /tmp/polybar_herbstluftwm.log
polybar herbstluftwm >>/tmp/polybar_herbstluftwm.log 2>&1 &

echo "Bar(s) launched.."
