#!/bin/bash
USERNAME=<your username here>
USERID=1000

export XAUTHORITY=/home/$USERNAME/.Xauthority
export DISPLAY=:0
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$USERID/bus

if [ "$(/usr/bin/id -u)" != "$USERID" ] ; then
    sudo -u $USERNAME XAUTHORITY=/home/$USERNAME/.Xauthority DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$USERID/bus /usr/bin/notify-send.py "$@"
else
    /usr/bin/notify-send.py "$@"
fi
