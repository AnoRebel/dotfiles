KeyBindings

# => Key Definitions
Mod4 = WINDOWS key or Super key
Mod1 = ALT key
Control = CTRL key
Shift = SHIFT key
Escape = ESCAPE key
Return = ENTER or RETURN key
KP_<key> = Keypad Keys
Pause = PAUSE key
Print = PRINT key
Tab = TAB key
space = Space Key (i3)
button1 = Mouse Left
button2 = Mouse Middle
button3 == Mouse Right

# => Bindings

UP == l
DOWN == k
LEFT == j
RIGHT == semicolon(;)

# Move focused container to workspace
Mod4+Ctrl+1 move container to workspace $ws1
Mod4+Ctrl+2 move container to workspace $ws2
Mod4+Ctrl+3 move container to workspace $ws3
Mod4+Ctrl+4 move container to workspace $ws4
Mod4+Ctrl+5 move container to workspace $ws5
Mod4+Ctrl+6 move container to workspace $ws6
Mod4+Ctrl+7 move container to workspace $ws7

# Move to workspace with focused container
Mod4+Shift+1 move container to workspace $ws1; workspace $ws1
Mod4+Shift+2 move container to workspace $ws2; workspace $ws2
Mod4+Shift+3 move container to workspace $ws3; workspace $ws3
Mod4+Shift+4 move container to workspace $ws4; workspace $ws4
Mod4+Shift+5 move container to workspace $ws5; workspace $ws5
Mod4+Shift+6 move container to workspace $ws6; workspace $ws6
Mod4+Shift+7 move container to workspace $ws7; workspace $ws7

Mod4+Shift+d == Dmenu

Mod4+m == Morc Menu

Alt+F2 == gmrun

Mod4+F11 == Rofi (Fullscreen)

Mod4+r == Rofi

Mod4+Shift+e == Logs out X session

Mod4+X / Control+Alt+Delete == Logs out of X session (Dialog Options)

Mod4+Ctrl+r == Restart WM
Mod4+Shift+r == Reload Configs
Mod4+Shift+q == Kill

Mod4+Left/j == Focus left
Mod4+Down/k == Focus down
Mod4+Up/l == Focus up
Mod4+Right/; == Focus right

Mod4+Shift+Left/j == Move left
Mod4+Shift+Down/k == Move down
Mod4+Shift+Up/l == Move up
Mod4+Shift+Right/; == Move right

Alt+Tab == Workspace back_and_forth
Alt+Ctrl+Right == Workspace next
Alt+Ctrl+Left == Workspace prev

Mod4+h == Split horizontal
Mod4+v == Split vertical
Mod4+q == Split toggle
Mod4+f == Fullscreen toggle
Mod4+s == Layout stacking
Mod4+z == Layout tabbed
Mod4+c == Layout toggle split

Mod4+Shift+space == Focus mode_toggle
Mod4+a == Focus parent
Mod4+d == Focus child
Alt+r == Mode "resize"
		# => "resize"
		=> j == resize shrink width 10 px or 10 ppt
    	=> k == resize grow height 10 px or 10 ppt
    	=> l == resize shrink height 10 px or 10 ppt
    	=> ; == resize grow width 10 px or 10 ppt

	    # same bindings, but for the arrow keys
	    => Left == resize shrink width 4 px or 4 ppt
	    => Down == resize grow height 4 px or 4 ppt
	    => Up == resize shrink height 4 px or 4 ppt
	    => Right == resize grow width 4 px or 4 ppt

	    # back to normal: Enter or Escape
	    => Return == mode "default"
	    => Escape == mode "default"

# => Workspace assignment

# Workspace 1 file managers
assign [class="Nemo|nemo|Caja|caja"]    → 1

# Workspace 2 text editor related
assign [class="Xed|xed|Gedit|gedit|Sublime|subl|Atom|atom|Code|code"]   → 2

# Workspace 3 browser related
assign [class="Firefox|firefox|Brave-Browser|brave-browser"]   → 3 

# Workspace 4 all video related software
assign [class="Vlc|vlc|Celluloid|celluloid"]    → 4

# Workspace 5 Images and meld
assign [class="feh|eog|Eog|Pinta|pinta"]    → 5

# Workspace 6 music related
assign [class="Rhythmbox|rhythmbox"]    → 6

# Workspace 7 email clients
assign [class="Thunderbird|thunderbird"]    → 7

# => Autostart Apps (Boot)

# Key Ring
/usr/bin/gnome-keyring-daemon --daemonize --login

# SSH Key Agent
/usr/bin/gnome-keyring-daemon --start --components=ssh

#file manager
alacritty -e ranger

#terminal
alacritty

#redshift
redshift-gtk

# Updater
mintupdate-launcher

# Mouse Cleanup
unclutter

# num lock activated
numlockx on

# => System Tray Apps

# bluetooth
blueberry-tray

# network
nm-applet

# volume
pasystray

# clipit
clipit

# => Not Workspace Related

#xfce
Control+Alt+m == xfce4-settings-manager
#cinnamon
Control+Alt+m == cinnamon-settings
#mate
Control+Alt+m == mate-control-center

# terminal
Mod4+Return == mate-terminal
Control+Alt+t == alacritty

# System monitor
Control+Shift+Escape == mate-system-monitor

# Fancy Lock
Control+Alt+k == i3lock-fancy

# Scratchpads
Mod4+Shift+u [title="dropdown"] scratchpad show; [title="dropdown"] move position center
Mod4+Ctrl+u == resize shrink height 10 px or 10 ppt

# Volume Control
Control+Alt+u == mate-volume-control

# start xfce-appfinder
Control+Alt+a == xfce4-appfinder

# Cmus music
Mod4+Shift+n == cmus

# Edit configs
Mod4+Shift+o == Edit Config

# xkill
Mod4+Ctrl+x == xkill

# => Workspace Related
#workspace 1 related
for_window [class="Nemo|Caja"] focus
Mod4+e == nemo
Control+Alt+b == caja

#workspace 2 related
Mod4+F2 == atom
Control+Alt+w == libreoffice
Control+Alt+v == code
Control+Alt+v == subl

#workspace 3 related
Mod4+w == exo-open --launch webbrowser
Control+Alt+f == firefox
Control+Alt+g == brave-browser

#workspace 4 related
Mod4+p == vlc
Mod4+p == celluloid

#workspace 5 related
Mod4+F5 == pix %U
Mod4+i == eog %U
Mod4+F5 == gimp

#workspace 6 related
Control+Alt+e == thunderbird

#workspace 7 related
Control+Alt+s == rhythmbox

# => Screenshots

Print == scrot '%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f$$(xdg-user-dir PICTURES)'
Control+Print == mate-screenshot
Control+Shift+Print == shutter
Shift+Print == mate-screenshot -i

# => Floating enabled from some programs (find with xprop)

for_window [class="Blueberry.py"] floating enable
for_window [class="Mintstick.py"] floating enable
for_window [class="Brasero"] floating disable
for_window [class="Gnome-calculator"] floating enable border pixel 1
for_window [class="Gnome-disks"] floating enable
for_window [class="^Gnome-font-viewer$"] floating enable
for_window [class="^Gpick$"] floating enable
for_window [class="Imagewriter"] floating enable
for_window [class="Font-manager"] floating enable
for_window [class="qt5ct|Lxappearance"] floating enable
for_window [class="PulseAudio"] floating disable
for_window [class="Usb-creator-gtk"] floating enable
for_window [class="vlc"] floating disable
for_window [class="^Celluloid$"] floating disable
for_window [class="Wine"] floating enable
for_window [class="Xfburn"] floating disable
for_window [class="Xfce4-settings-manager"] floating enable
for_window [class="mate-control-center"] floating enable
for_window [class="mate-notification-daemon"] floating enable
for_window [class="Xfce4-taskmanager"] floating enable
for_window [instance="gimp"] floating enable
for_window [instance="script-fu"] border normal
for_window [title="Copying"] floating enable
for_window [title="Deleting"] floating enable
for_window [title="Moving"] floating enable
for_window [window_role="^gimp-toolbox-color-dialog$"] floating enable
for_window [window_role="pop-up"] floating enable
for_window [window_role="^Preferences$"] floating enable
for_window [window_role="setup"] floating enable
for_window [title="i3_help"] floating enable sticky enable border normal
for_window [class="GParted"] floating enable border normal
for_window [class="Lightdm-settings"] floating enable
for_window [class="Simple-scan"] floating enable border normal

# => Audio Setup

XF86AudioRaiseVolume == "amixer -D pulse sset Master '5%+'"
XF86AudioLowerVolume == "amixer -D pulse sset Master '5%-'"
XF86AudioMute == "amixer -D pulse set Master toggle"
#https://github.com/acrisci/playerctl/
XF86AudioPlay == playerctl play-pause
XF86AudioNext == playerctl next
XF86AudioPrev == playerctl previous
XF86AudioStop == playerctl stop

# => XBacklight (Screen Brightness)

#increase screen brightness
XF86MonBrightnessUp == xbacklight -inc 20
#decrease screen brightness
XF86MonBrightnessDown == xbacklight -dec 20

# => Extra Keys

XF86Reload == restart
XF86TouchpadToggle == toggletouchpad
XF86TouchpadOn == synclient TouchpadOff=0
XF86TouchpadOff == synclient TouchpadOff=1
XF86Close == kill
XF86Display == displayselect
XF86WLAN == $netrefresh
(i3-Gaps)
Mod4+Shift+f == gaps inner current set 0; gaps outer current set 0

# => Bar toggle, hide or show
Mod4+b == bar mode toggle

# => Border control (i3)
Mod4+shift+b == i3-msg border toggle

#changing border style
Mod4+t == border normal
Mod4+y == border 1pixel
Mod4+u == border none

# => i3-Gaps

set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
Mod4+Shift+g == mode "$mode_gaps"
			    => o == mode "$mode_gaps_outer"
			    => i == mode "$mode_gaps_inner"
			    => Return == mode "default"
			    => Escape == mode "default"
							# => "$mode_gaps_inner"
							    plus == gaps inner current plus 5
							    minus == gaps inner current minus 5
							    0 == gaps inner current set 0

							    Shift+plus == gaps inner all plus 5
							    Shift+minus == gaps inner all minus 5
							    Shift+0 == gaps inner all set 0

							    Return == mode "default"
							    Escape == mode "default"
							# => "$mode_gaps_outer"
							    plus == gaps outer current plus 5
							    minus == gaps outer current minus 5
							    0 == gaps outer current set 0

							    Shift+plus == gaps outer all plus 5
							    Shift+minus == gaps outer all minus 5
							    Shift+0 == gaps outer all set 0

							    Return == mode "default"
							    Escape == mode "default"

# => Compositor
picom --config ~/.config/i3/picom.conf --experimental-backends

# => Workspace Toggle(Switch) (i3-Gaps)
workspace_auto_back_and_forth yes
Mod4+Tab workspace back_and_forth
Mod4+Shift+Tab move container to workspace back_and_forth; workspace back_and_forth

# => Mouse Movements
Mod4+button3 == floating toggle
Mod4+button1 == move floating
Mod4+button3 == resize floating

# The side buttons move the window around
button9 == move left
button8 == move right


Key([modkey], "h", lazy.layout.left()),
Key([modkey], "l", lazy.layout.right()),
Key([modkey], "j", lazy.layout.down()),
Key([modkey], "k", lazy.layout.up()),

Key([modkey, "shift"], "h", lazy.layout.swap_left()),
Key([modkey, "shift"], "l", lazy.layout.swap_right()),
Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
Key([modkey, "shift"], "j", lazy.layout.shuffle_down()),
Key([modkey, "shift"], "k", lazy.layout.shuffle_up()),

Key([modkey], "i", lazy.layout.grow()),
Key([modkey], "m", lazy.layout.shrink()),
Key([modkey, "shift"], "space", lazy.layout.flip()),

Key([mod, "control"], "j", lazy.layout.grow_down()),
Key([mod, "control"], "k", lazy.layout.grow_up()),
Key([mod, "control"], "h", lazy.layout.grow_left()),
Key([mod, "control"], "l", lazy.layout.grow_right()),
Key([mod], "Return", lazy.layout.toggle_split()),

Key([mod, "mod1"], "h", lazy.layout.flip_left()),
Key([mod, "mod1"], "l", lazy.layout.flip_right()),
Key([mod, "mod1"], "j", lazy.layout.flip_down()),
Key([mod, "mod1"], "k", lazy.layout.flip_up()),