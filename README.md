<div align="center">
    <h1>Awesome Dotfiles</h1>
</div>

## Screenshots
![Info](./screenshot.jpg "Info Setup")

![Desktop](./screenshot1.jpg "Desktop")

![Browser](./screenshot2.jpg "Browser")


## Contents ##
1. [Details](#details)
2. [Features](#features)
3. [Dependencies](#dependencies)
4. [Installation](#installation)
5. [Folder Structure](#folderStructure)
6. [My Preferred Applications](#applications)
7. [Application Theming](#appTheming)
8. [Keybinds](#keybinds)

<a name="details"></a>
## Details ##
+ **OS**: Linux Mint
+ **Shell**: ZSH
+ **WM**: Bspwm/i3-gaps(previously)
+ **Theme**: Adapta Nokto
+ **Icons**: Candy Icons
+ **Cursor**: Breeze Dark
+ **Terminal**: Kitty/Alacritty(previously)

<a name="features"></a>
## Features ##
+ Lightweight
+ Easy installation / configuration
+ Very few dependencies
+ Locking (via `betterlockscreen`)
+ Very stable

<a name="dependencies"></a>
## Dependencies ##

|     Dependency     |                                  Description                                  |
|:------------------:|:-----------------------------------------------------------------------------:|
|      `bspwm`       |                                Window manager                                 |
|     `i3-gaps`      |                          Window manager(previously)                           |
|       `feh`        |              Fast image viewer used as wallpaper setting utility              |
|  `compton/picom`   | Window compositor, eliminates screen tearing and allows for cool fade effects |
|     `polybar`      |                    Awesome highly configurable status bar                     |
|       `rofi`       |                             Application launcher                              |
|      `dmenu`       |                         Another application launcher                          |
| `betterlockscreen` |                            Used to lock the screen                            |
|   `imagemagick`    |            **OPTIONAL BUT NEEDED IF USING A DIFFERENT BACKGROUND**            |
|                    |                                                                               |

### Optional Dependencies ###
These will improve the user experience but aren't required:
**Bear in mind that most of these dependencies come preinstalled on some systems. I would recommend reading their descriptions below to determine which ones you need to install. Alternatively, set up my config and install the packages based on what isn't working.**
+ `betterlockscreen`: Configurable beautiful lockscreen
+ `acpi`: Battery managing cli application
+ `xfce4-power-manager`: Lightweight power manager but I use `mate-power-manager` because I have MATE DE installed too
+ `blueman`: Bluetooth managing application, spawns when the bluetooth top panel icon is clicked
+ `mintupdate`: Default GUI package manager, spawned when the top panel package icon is clicked
+ `nm-connection-editor`: GUI wifi connection editor, spawned when the top panel wifi icon is clicked
+ `scrot`: Screenshot tool, which is mapped to the Print Screen key `.config/i3/config` or `.config/sxhkd/sxhkdrc`(for bspwm). **If you want to meet this dependency, ensure that the `~/Pictures` folder exists**, otherwise the program will save your screenshots to your home directory
+ `alsa-utils or pactl`: Provides kernel driven sound drivers, which the control of has been mapped to volume keys in `.config/i3/config` or `.config/sxhkd/sxhkdrc`(for bspwm)
+ `xbacklight`: Controls display brightness, which the control of has been mapped to brightness keys in `.config/i3/config` or `.config/sxhkd/sxhkdrc`(for bspwm)

### Fonts You Should Install ###
+ `Nerd Fonts`: Used in this config at different areas. I cant list them all here.
+ `Powerline Fonts`: Used in this config as the terminal(airline) fonts.
+ `Font Awesome 4/5`: To get polybar icons to show.

<a name="installation"></a>
## Installation ##
1. Ensure all [dependencies](#dependencies) are met
2. Clone this repository and place its contents into their respective location folder
3. edit each `config` file to your desires.
4. optional: edit the `.config/i3/config` or `.config/sxhkd/sxhkdrc`(for bspwm) file to change / add keybinds
5. optional: edit the `.config/polybar/config` file to change / configure your polybar setup

<a name="applications"></a>
## My Preferred Applications ##
+ **Display Manager - LightDM**: Beautiful, simple and lightweight display manager.
+ **Text Editor - VSCode, Sublime Text and Neovim**: Each for their own purpose.
+ **File Manager - Nemo/Caja/PcManFM/Ranger**: Lightweight file browser, few dependencies, and can be configured to work with a preferred terminal. Also has extensions for easy right click extraction / compression of archive files. (ie zip / rar etc)
+ **Web Browser - Brave and Firefox**: Super configurable and isn't made by Google.(Though brave uses the chromium engine but it saves me alot of unneccessary ads and popups)
+ **Terminal - Kitty and Alacritty**: I started out with Alacritty then I met kitty and its built-in tabs, panes and windows, so now i have both, just-in-case.
+ **Music - GUI (Rhythmbox), CLI (cmus/ncmpcpp(mpd))**: I mostly use cmus since its so easy to startup as I'm working on the shell.
+ **Video - Xplayer/Celluloid(mpv)/VLC**: Lately I use xplayer due to a GPU bug that crashes my system if prolonged use of either VLC,Celluloid/MPV without passing the `--vo=vdpau` flag in Celluloid and MPV or changing the renderer to X in VLC.
+ **Theme / Look & Feel Manager - Mate settings or lxappearance**: makes managing icon / cursor / application themes easy, only theme manager with no DE(lxappearance) dependencies, and works very well.

### Other cool applications you should install ###
+ `redshift`: Changed screen warmth based on the time of day
+ `neofetch`: Displays system information in the terminal
+ `cmus`: Terminal audio player!
+ `ncmpcpp(mpd)`: Terminal audio player!
+ `cava`: Terminal audio visualizer!
+ `vis`: Terminal audio visualizer!

<a name="appTheming"></a>
## Application Theming ##
### Neovim/Vim ###
1. Ensure the nvim folder from the repo has been copied into the `~/.config` directory
2. Open neovim and the config is setup to automatically install `vimplug` and the plugins, so read the `.vimrc` first!
3. Exit and reopen neovim

### Zsh ###
1. Install oh-my-zsh
```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```
2. Change the zsh theme to powerlevel10k
  + Open `~/.zshrc` with your fave text editor
  + Set `ZSH_THEME="agnoster"` and save the file
3. Install Plugins (Note that the ~/.zshrc edits are already done in this repo)
  + Syntax highlighting (copy and paste the below command to install)
    ```
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    ```
    + Edit `~/.zshrc`, add `zsh-syntax-highlighting` to the plugins section
  + Autosuggestions (copy and paste the below command to install)
    ```
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    ```
    + Edit `~/.zshrc`, add `zsh-autosuggestions` to the plugins section
4. Done! Reopen the terminal and voila.
5. 
<a name="keybinds"></a>
## Keybinds ##
**Note that the modkey is set to be the windows / command key. If you would like to use a different modkey check out the `keys.lua` file.**
If you are new to awesomewm, note that tag refers to workspace, and client refers to window.

### Keyboard ###
+ `mod + alt + t`: Spawn terminal (Kitty or Alacritty)
+ `mod + enter`: Spawn terminal (mate-terminal)
+ `mod + d`: Open file manager (Nemo or Caja)
+ `mod + r`: Spawn rofi (an application menu)
+ `mod + d`: Spawn dmenu (another application menu)
+ `mod + f`: Make client fullscreen
+ `mod + space`: Toggle floating mode
+ `mod + [1-7]`: Switch to tag [1-7]
+ `mod + shift + [1-7]`: Move client and focus to tag [1-7]
+ `mod + control + [1-7]`: Move client to tag [1-7]
+ `alt + tab`: Change the tag layout, alternating between all available layouts
+ `mod + [up / down / left / right / h / j / k / l]`: Change client by direction
+ `mod + Shift + [up / down / left / right / h / j / k / l]`: Move client by direction
+ `mod + Control + [up / down / left / right / h / j / k / l]`: Resize client by direction
+ `control + alt + k`: Lock screen
+ `mod + shift + q`: Kill focused window
+ `mod + x`: Show system exit popup
+ `mod + shift + r`: Reload WM
+ `mod + control + r`: Restart WM
+ `mod + shift + e`: Logout

### Mouse ###
+ `mod + drag with left click`: Move floating client
+ `mod + drag with right click`: Resize floating client

