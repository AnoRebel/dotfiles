#!/bin/bash

echo "Removing any other instance of bootiso.."

sudo rm /usr/bin/bootiso

cd

echo "Downloading bootiso in the download folder.."

cd ~/Downloads

curl -L https://git.io/bootiso -O

echo "Setting permissions.."

chmod +x bootiso

sudo mv bootiso /usr/bin

echo "Now just run 'bootiso' anywhere on your terminal.."
